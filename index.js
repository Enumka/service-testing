const testService = (events) => {
  if (!events.length) return true;

  if (events.length % 2) return false;

  const visitors = {};

  const STATUS_OUT = 'out';

  for (const event of events) {
    const [name, status] = event;

    const isTwiceSameAction = visitors[name] === status; // Ситуация, когда человек уже записан в системе со статусом и натыкаемся на событие с таким же статусом(дважды вход, дважды выход)

    const isOutUnknown = status === STATUS_OUT && !visitors[name]; //Ситуация, когда выходит человек, который не входил

    if (isTwiceSameAction || isOutUnknown) return false;

    visitors[name] = status;
  }

  return Object.values(visitors).every((status) => status === STATUS_OUT); // Смотрич, что все посетители, которых регистрировал датчик вышли
};

module.exports = testService;
